from abc import ABCMeta, abstractmethod

class Storage:
    __metaclass__ = ABCMeta

    @abstractmethod
    def add_product(self):
        raise NotImplementedError()

    @abstractmethod
    def add_retailer(self):
        raise NotImplementedError()
        
    @abstractmethod
    def add_product(self, name, category, photo = ''):
        raise NotImplementedError()

    @abstractmethod
    def add_retailer(self, name, products):
        raise NotImplementedError()        
        
    @abstractmethod
    def get_retailers(self, product_id):
        raise NotImplementedError()
    
    @abstractmethod
    def add_category(self, category):
        raise NotImplementedError()

    @abstractmethod
    def get_products(self, category):
        raise NotImplementedError()
    
    @abstractmethod
    def get_product(self, p_id):
        raise NotImplementedError()
        
    @abstractmethod
    def get_reviews(self, p_id):
        raise NotImplementedError()
        
    @abstractmethod
    def get_categories(self):
        raise NotImplementedError()
        
    @abstractmethod
    def get_category(self, cat_id):
        raise NotImplementedError()
