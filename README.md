# README

 - Product
   * id
   * Name
   * Photo (URL?)
   * Consumer reviews (lista av reviews)
   * Category
   
 - Category
   * id
   * Name 

 - Retailer
   * id
   * Name
   * (Products (id), Price)

 - Review
   * Description
   * Rating 1-5

Example product:
{
   "id":0,
   "name":"Stol",
   "photo":"http://data.com/stol.jpegsson",
   "reviews":[
      {
         "description":"Skön att sitta på",
         "rating":4
      },
      {
         "description":"Oskön att sitta på",
         "rating":1
      }
   ],
   "category":"Möbler"
}
	
Example retailer:
{
   "id":0,
   "name":"IKEA",
   "products":[
      {
         "id":0,
         "price":3000
      }
   ]
}


	
