from flask import Flask
from flask import render_template
from flask import request
from jsonstorage import JSONStorage
import sys

app = Flask(__name__)

@app.route('/')
def start_page():
    return render_template('prisyacht.html', categories=storage.get_categories())

@app.route('/products')
@app.route('/categories/<category>')
def category_page(category=None):
    return render_template('category.html', category=storage.get_category(category), products=storage.get_products(category))

@app.route('/products/<product>', methods=['GET', 'POST'])
def product_page(product=None):
    if request.method == 'POST':
        storage.add_review(product, request.form['rating'], request.form['description'])
    return render_template('product.html', product=storage.get_product(product), retailers=storage.get_retailers(product), reviews=storage.get_reviews(product))

@app.route('/add_retailer', methods=['GET', 'POST'])
def add_retailer():
    if request.method == 'POST':
        keys = ["%s.check"%x for x in range(len(storage.get_products()))]
        products = []
        for key in keys:
            try:
                if request.form[key] == 'on':
                    price = request.form[key.replace("check", "price")]
                    products.append({'id': key.split('.')[0], 'price': price})
            except KeyError:
                pass
        storage.add_retailer(request.form['name'], products)
    return render_template('add_retailer.html', retailers=storage.get_retailers(), products=storage.get_products())

@app.route('/add_product', methods=['GET', 'POST'])
def add_product():
    if request.method == 'POST':
        storage.add_product(request.form['name'], request.form['category'], request.form['photo'])
    return render_template('add_product.html', categories=storage.get_categories())

@app.route('/add_category', methods=['GET', 'POST'])
def add_category():
    if request.method == 'POST':
        storage.add_category(request.form['name'])
    return render_template('add_category.html', categories=storage.get_categories())

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print "Usage: %s retailers_file products_file categories_file"% sys.argv[0]
        sys.exit(-1)
    storage = JSONStorage(sys.argv[1], sys.argv[2], sys.argv[3])
    app.run(host='0.0.0.0')
