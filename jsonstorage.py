from storage import Storage
from operator import itemgetter
import numpy
import json
import sys

class JSONStorage(Storage):
    def __init__(self, retailers_file, products_file, categories_file):
        self.__retailers_file = retailers_file
        self.__products_file = products_file
        self.__categories_file = categories_file
        self.__load_all()

    def add_product(self, name, category, photo = ''):
        self.__load_products()
        new_id = len(self.__products)
        product = {'id': new_id, 'name': name, 'category': category, 'reviews': [], 'photo': photo}
        self.__products.append(product)
        self.__save_to_file(self.__products, self.__products_file)
        
    def add_review(self, product_id, rating, description):
        self.__load_products()
        review = {'rating': rating, 'description': description}
        self.__products[int(product_id)]['reviews'].append(review)
        self.__save_to_file(self.__products, self.__products_file)

    def add_retailer(self, name, products):
        self.__load_retailers()
        new_id = len(self.__retailers)
        retailer = {'id': new_id, 'name': name, 'products': products}
        self.__retailers.append(retailer)
        self.__save_to_file(self.__retailers, self.__retailers_file)
        
    def get_retailers(self, product_id = None):
        self.__load_retailers()
        if product_id == None:
            return self.__retailers
        retailers = []
        for r in self.__retailers:
            for p in r['products']:
                if p['id'] == product_id:
                    retailers.append({'name': r['name'], 'price': p['price']})
                    break
        return sorted(retailers, key=itemgetter('price'))
    
    def add_category(self, category):
        self.__load_categories()
        cat = {'name': category, 'id': len(self.__categories)}
        self.__categories.append(cat)
        self.__save_to_file(self.__categories, self.__categories_file)

    def get_products(self, category = None):
        self.__load_products()
        if category == None:
            products = self.__products
        else:
            products = [x for x in self.__products if x['category'] == category]
        for p in products:
            l = [float(x['rating']) for x in p['reviews']]
            p['rating'] = numpy.mean(l)
        return sorted(products, key=itemgetter('rating'), reverse=True)
    
    def get_product(self, p_id):
        self.__load_products()
        return self.__products[int(p_id)]
        
    def get_reviews(self, p_id):
        self.__load_products()
        return self.get_product(p_id)['reviews']
        
    def get_categories(self):
        self.__load_categories()
        return self.__categories
        
    def get_category(self, cat_id):
        self.__load_categories()
        if cat_id == None:
            return None
        return self.__categories[int(cat_id)]
    
    def __load_all(self):
        self.__load_products()
        self.__load_retailers()
        self.__load_categories()
    
    def __load_products(self):
        f = open(self.__products_file)
        self.__products = json.load(f)
        f.close()

    def __load_retailers(self):
        f = open(self.__retailers_file)
        self.__retailers = json.load(f)
        f.close()
        
    def __load_categories(self):
        f = open(self.__categories_file)
        self.__categories = json.load(f)
        f.close()
        
    def __save_to_file(self, data, filename):
        f = open(filename, 'w')
        json.dump(data, f)
        f.close()
